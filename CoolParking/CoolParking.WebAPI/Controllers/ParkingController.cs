﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : Controller
    {
        private static ParkingService parkingService;

    public ParkingController(ParkingService parkServ)
    {
        parkingService = parkServ;
    }

        [HttpGet("balance")]
        public ActionResult<decimal> Get()
        {
            return parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
