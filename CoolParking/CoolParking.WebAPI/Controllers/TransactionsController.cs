using System;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        private static ParkingService parkingService;

    public TransactionsController(ParkingService parkServ)
    {
        parkingService = parkServ;
    }

        [HttpGet("last")]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            return parkingService.GetLastParkingTransactions();
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return parkingService.ReadFromLog();
            }
            catch (Exception)
            {
                return NotFound();
            }
            
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TopUpTransaction newTransaction)
        {
            if ((!Regex.IsMatch(newTransaction.Id, @"^[A-Z]{2}\-\d{4}\-[A-Z]{2}$")) || newTransaction.Sum < 0)
                return BadRequest();
            try
            {
                parkingService.TopUpVehicle(newTransaction.Id, newTransaction.Sum);
                return Parking.Instance.vehicles.Find(v => v.Id == newTransaction.Id);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }
    }
}