﻿using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CoolParkingClient
{
    class MenuProvider
    {
        static HttpClient client = new HttpClient();
        public static bool toContinue { get; private set; } = true;
        private static Action[] MenuActions;
        readonly static string MenuOptions;

        static MenuProvider()
        {
            client.BaseAddress = new Uri("https://localhost:5001/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            MenuOptions = "Choose an action: \n" +
                "0. Exit \n" +
                "1. Get vehicles on the parking \n" +
                "2. Top up vehicle \n" +
                "3. Add vehicle \n" +
                "4. Parking balance \n" +
                "5. Free places \n" +
                "6. Get all parking transactions \n" +
                "7. Get last parking transacions \n" +
                "8. Remove vehicle \n" +
                "9. Get vehicle by ID";
            MenuActions = new Action[] { Exit, GetVehicles, TopUpVehicle, AddVehicle, GetParkingBalance, GetParkingFreePlaces, GetAllTransactions,
                GetLastTransactions, RemoveVehicle, GetVehicleById};
        }
        public static void ProvideMenu()
        {
            Console.WriteLine(MenuOptions);
            Console.Write("Your input : ");

            try
            {
                var userInput = Convert.ToInt32(Console.ReadLine());
                MenuActions[userInput]();
                Console.WriteLine();
            }
            catch (FormatException)
            {
                Console.WriteLine("Your input is not a number.");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine($"Error. Please enter a number between 0 and {MenuActions.Length}.");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }

        }

        private static void Exit()
        {
            toContinue = false;
        }

        private static void GetVehicles()
        {
            HttpResponseMessage response = client.GetAsync("api/vehicles").Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. {response.StatusCode}");
                return;
            }

            ReadOnlyCollection<Vehicle> vehiclesCollection = response.Content.ReadAsAsync<ReadOnlyCollection<Vehicle>>().Result;
            Console.WriteLine($"There are {vehiclesCollection.Count} vehicles on the parking.");
            foreach (var vehicle in vehiclesCollection)
            {
                Console.WriteLine(vehicle);
            }
        }

        private static void TopUpVehicle()
        {
            try
            {
                Console.Write("Enter vehicle ID (case sensitive, in format AB-1234-CD)  : ");
                var id = Console.ReadLine();
                Console.Write("Enter top up sum : ");
                var sum = Convert.ToInt32(Console.ReadLine());
                var response = client.PutAsJsonAsync($"api/transactions/topUpVehicle",
                    new TopUpTransaction { Id = id, Sum = sum }).Result;
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't top up vehicle. Status code: {response.StatusCode}");
                    return;
                }
                var vehicle = response.Content.ReadAsAsync<Vehicle>().Result;
                Console.WriteLine($"Status code: {response.StatusCode}");
                Console.WriteLine($"Vehicle {vehicle.Id} has been topped up. New balance: {vehicle.Balance}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private static void AddVehicle()
        {
            Vehicle addedVehicle;
            Console.WriteLine("\nChoose vehicle type : ");
            Console.WriteLine("0 - Passanger car");
            Console.WriteLine("1 - Truck");
            Console.WriteLine("2 - Bus");
            Console.WriteLine("3 - Motorcycle");
            try
            {
                var vehicleTypeInputString = Console.ReadLine();
                var vehicleTypeInput = Convert.ToInt32(vehicleTypeInputString);
                var addedVehicleType = (VehicleType)(vehicleTypeInput);

                Console.Write("Enter vehicle ID (case sensitive, in format AB-1234-CD)  : ");
                var vehicleId = Console.ReadLine();

                Console.WriteLine("\nEnter vehicle balance : ");
                var vehicleBalanceInput = Convert.ToDecimal(Console.ReadLine());
                addedVehicle = new Vehicle(vehicleId, addedVehicleType, vehicleBalanceInput);

                HttpResponseMessage response = client.PostAsJsonAsync("api/vehicles", addedVehicle).Result;
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Couldn't add vehicle. Status: {response.StatusCode}");
                    return;
                }
                Console.WriteLine("Status code: " + response.StatusCode);
                Console.WriteLine("Added vehicle with ID = " + addedVehicle.Id);

            }
            catch (FormatException)
            {
                Console.WriteLine("Your input is not a number!");
            }

            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }
            ////TODO: input of vahicle
            //// ======================
            //var vehi = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Bus, 300);
            //// ==================

        }
        private static void GetParkingBalance()
        {
            var response = client.GetAsync("api/parking/balance").Result;
            var balance = response.Content.ReadAsAsync<decimal>().Result;
            Console.WriteLine($"Parking balance = {balance}");
        }
        //private static void ParkingCapacity()
        //{
        //    var response = client.GetAsync("api/parking/capacity").Result;
        //    var capacity = response.Content.ReadAsAsync<int>().Result;
        //    Console.WriteLine("Parking capacity = " + capacity);
        //}

        private static void GetParkingFreePlaces()
        {
            var response = client.GetAsync("api/parking/freeplaces").Result;
            var places = response.Content.ReadAsAsync<int>().Result;

            response = client.GetAsync("api/parking/capacity").Result;
            var capacity = response.Content.ReadAsAsync<int>().Result;
            Console.WriteLine($"There are {places} free places out of {capacity}");
        }

        private static void GetAllTransactions()
        {
            var response = client.GetAsync("api/transactions/all").Result;
            var transactions = response.Content.ReadAsAsync<string>().Result;
            Console.WriteLine(transactions);
        }

        private static void GetLastTransactions()
        {
            var response = client.GetAsync("api/transactions/last").Result;
            var transactions = response.Content.ReadAsAsync<TransactionInfo[]>().Result;
            foreach (var transaction in transactions)
                Console.WriteLine(transaction);
        }

        private static void RemoveVehicle()
        {
            Console.Write("Enter vehicle ID (case sensitive, in format AB-1234-CD)  : ");
            var id = Console.ReadLine();
            var response = client.DeleteAsync($"api/vehicles/{id}").Result;
            Console.WriteLine($"Status code: {response.StatusCode}");
        }

        private static void GetVehicleById()
        {
            Console.Write("Enter vehicle ID (case sensitive, in format AB-1234-CD)  : ");
            var id = Console.ReadLine();
            var response = client.GetAsync($"api/vehicles/{id}").Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error. Status code: {response.StatusCode}");
                return;
            }
            Console.WriteLine($"Status code: {response.StatusCode}");
            var vehicle = response.Content.ReadAsAsync<Vehicle>().Result;
            Console.WriteLine(vehicle);
        }
    }
}
