﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        /// <summary>
        /// A path to the file that is going to be used for logging.
        /// </summary>
        public string LogPath { get; set; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        /// <summary>
        /// A stream for a file, supporting read and write operations.
        /// </summary>
        FileStream fileStream;

        public LogService(string _logfilePath)
        {
            LogPath = _logfilePath;
            if (File.Exists(LogPath))
                fileStream = File.Create(LogPath);
            fileStream?.Dispose();
        }

        public LogService()
        {
            if (File.Exists(LogPath))
                fileStream = File.Create(LogPath);
            fileStream?.Dispose();
        }
        /// <summary>
        /// Reads all the text from the specified file if this file exists.
        /// </summary>
        /// <returns></returns>
        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("Ошибка! Заданный файл для считывания не найден.");
            return File.ReadAllText(LogPath);
        }
        /// <summary>
        /// Writes the given string to the specified file. If the file does not exist, it will be created.
        /// </summary>
        /// <param name="logInfo"></param>
        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = new StreamWriter(LogPath, append: true))
            {
                streamWriter.WriteLine(logInfo);
            }
        }
    }
}
