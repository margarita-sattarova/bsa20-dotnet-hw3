﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        /// <summary>
        /// Stores the list of current Parking transactions.
        /// </summary>
        List<TransactionInfo> transactions;
        /// <summary>
        /// An instance of Log Service responsible for logging transactions to the file.
        /// </summary>
        ILogService Log;
        ITimerService WithdrawTimer, Logtimer;
        /// <summary>
        /// Current Parking balance (before recording to the log file).
        /// </summary>
        public decimal currentParkingIncome = 0;
        private Parking _instance;

        public ParkingService(ITimerService withdrawTimer, ITimerService logtimer, ILogService logService)
        {
            transactions = new List<TransactionInfo>();
            Log = logService;
            WithdrawTimer = withdrawTimer;
            Logtimer = logtimer;
            
            WithdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            Logtimer.Elapsed += Logtimer_Elapsed;
            
            WithdrawTimer.Start();
            Logtimer.Start();

            _instance = Parking.Instance;
        }
        /// <summary>
        /// Log Timer Elapsed event handler. Writes to the log file all transactions in the list of current transactions, after logging clears this list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Logtimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();            
            lock (this)
            {
                foreach (var trans in GetLastParkingTransactions())
                {
                    stringBuilder.AppendLine(trans.ToString());
                }
                transactions.Clear();
                currentParkingIncome = 0;
            }

            Log.Write(stringBuilder.ToString());            
        }
        /// <summary>
        /// Withdraw Timer Elapsed event handler. Creates a new transaction and adds it to the list of current Parking transactions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WithdrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (this)
            {
                foreach (var vehicle in Parking.Instance.vehicles)
                {
                    decimal sumToPay = CalculateFee(vehicle);
                    vehicle.Balance -= sumToPay;
                    TopUpParkingBalance(sumToPay);
                    currentParkingIncome += sumToPay;
                    {
                        transactions.Add(new TransactionInfo 
                        { 
                            TransactionDate = e?.SignalTime ?? DateTime.Now, 
                            Sum = sumToPay, 
                            vehicleId = vehicle.Id });
                    }
                }
            }            
        }
        /// <summary>
        /// Calculates the amount of money to be withdrawn from the vehicle balance based on the amount of money left.
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        decimal CalculateFee(Vehicle vehicle)
        {
            decimal fee = 0;
            decimal balance = vehicle.Balance;
            switch(vehicle.VehicleType)
            {
                case VehicleType.PassengerCar:
                    fee = Settings.passengerCarFee;
                    break;
                case VehicleType.Truck:
                    fee = Settings.truckFee;
                    break;
                case VehicleType.Bus:
                    fee = Settings.busFee;
                    break;
                case VehicleType.Motorcycle:
                    fee = Settings.motorcycleFee;
                    break;
            }

            if (balance > fee)
            {
                return fee;
            }
            else if (balance <= 0)
                return Settings.penaltyCoefficient * fee;

            // Vehicle currently has no debt but balance is not enough for payment
            // So penalty coefficient is applied only to lacking part of payment
            return balance + Settings.penaltyCoefficient * (fee - balance);
        }
        /// <summary>
        /// Adds the money withdrawn from vehicles on the parking to the parking balance.
        /// </summary>
        /// <param name="sum"></param>
        void TopUpParkingBalance(decimal sum) => Parking.Instance.balance += sum;
        /// <summary>
        /// Adds vehicle to the parking if there is no vehicle with the same id on the Pakring, and if there are free places at the Parking.
        /// </summary>
        /// <param name="vehicle"></param>
        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.Instance.vehicles.Exists(v => v.Id == vehicle.Id))
                throw new ArgumentException("User entered invalid Id!");
            if (GetFreePlaces() <= 0)
                throw new InvalidOperationException("There are no free places at the parking. Sorry");
            Parking.Instance.vehicles.Add(vehicle);
            Console.WriteLine($"Your vehicle is added to the parking (Vehicle ID = {vehicle.Id})");
        }
        /// <summary>
        /// Releases the resources allocated for timers and Parking.
        /// </summary>
        public void Dispose()
        {
            WithdrawTimer.Dispose();
            Logtimer.Dispose();
            _instance.ResetParking();
        }
        /// <summary>
        /// Returns the amount of money on the Parking balance.
        /// </summary>
        /// <returns></returns>
        public decimal GetBalance()
        {
            return Parking.Instance.balance;
        }
        /// <summary>
        /// Returns the total number of places at the Parking.
        /// </summary>
        /// <returns></returns>
        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }
        /// <summary>
        /// Returns the number of places left at the Parking.
        /// </summary>
        /// <returns></returns>
        public int GetFreePlaces()
        {
            return (Settings.parkingCapacity - Parking.Instance.vehicles.Count);
        }
        /// <summary>
        /// Returns an array of current Parking transactions (before logging to the file).
        /// </summary>
        /// <returns></returns>
        public TransactionInfo[] GetLastParkingTransactions() => transactions.ToArray();
        /// <summary>
        /// Returns the list of vehicles on the Parking.
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Instance.vehicles);
        }
        /// <summary>
        /// Returns the list of all Parking transactions.
        /// </summary>
        /// <returns></returns>
        public string ReadFromLog()
        {
            return Log.Read();
        }
        /// <summary>
        /// Removes vehicle from the Parking if the id is valid, and the balance of the vehicle is not a negative number.
        /// </summary>
        /// <param name="vehicleId"></param>
        public void RemoveVehicle(string vehicleId)
        {
            var indexOfVehicle = Parking.Instance.vehicles.FindIndex(v => v.Id == vehicleId);

            if (indexOfVehicle == -1)
                throw new ArgumentException();
            if (Parking.Instance.vehicles[indexOfVehicle].Balance < 0)
                throw new InvalidOperationException();

            Parking.Instance.vehicles.RemoveAt(indexOfVehicle);
            Console.WriteLine($"Your vehicle is removed from the parking");
        }
        /// <summary>
        /// Put the given sum on the balance of the vehicle.
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="sum"></param>
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
                var index = Parking.Instance.vehicles.FindIndex(v => v.Id == vehicleId);
                if (index == -1)
                    throw new ArgumentException("Error! Vehicle with such ID is not found at the parking.");
                if (sum <= 0)
                    throw new ArgumentException("Error! You cannot top up your vehicle' balance with non-positive amount of money.");
                Parking.Instance.vehicles[index].Balance += sum;
        }
    }
}