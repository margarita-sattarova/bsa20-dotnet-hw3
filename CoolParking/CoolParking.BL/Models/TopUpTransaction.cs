﻿using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class TopUpTransaction
    {
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}