﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonProperty("vehicleId")]
        public string vehicleId  { get; set; }
        /// <summary>
        /// Stores the sum of the transaction.
        /// </summary>
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        /// <summary>
        /// Stores the date and time when transaction was made.
        /// </summary>
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
        /// <summary>
        /// Returnes the formatted transaction record.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{TransactionDate} | Vehicle Id : {vehicleId} | Amount of money withdrawn: {Sum:N2}";

    }
}