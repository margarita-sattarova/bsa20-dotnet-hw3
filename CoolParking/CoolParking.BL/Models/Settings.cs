﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal initialParkingBalance = 0;
        /// <summary>
        /// The number of places available at the Parking.
        /// </summary>
        public const int parkingCapacity = 10;
        /// <summary>
        /// Payment withdrawal period.
        /// </summary>
        public const int paymentTimeInterval = 5;
        /// <summary>
        /// Logging period.
        /// </summary>
        public const int logTimeInterval = 60;
        /// <summary>
        /// The amount of money that has to be withdrawn from the vehicle of type Passenger Car.
        /// </summary>
        public const int passengerCarFee = 2;
        /// <summary>
        /// The amount of money that has to be withdrawn from the vehicle of type Truck.
        /// </summary>
        public const int truckFee = 5;
        /// <summary>
        /// The amount of money that has to be withdrawn from the vehicle of type Bus.
        /// </summary>
        public const decimal busFee = 3.5M;
        /// <summary>
        /// The amount of money that has to be withdrawn from the vehicle of type Motorcycle.
        /// </summary>
        public const int motorcycleFee = 1;
        /// <summary>
        /// Coefficient that is applied if the amount of money on the vehicle balance is not enough for the regular withdrawal.
        /// </summary>
        public const decimal penaltyCoefficient = 2.5M;
    }
}
